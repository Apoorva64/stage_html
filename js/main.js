//TODO: specify units
//TODO: make rabis generalisee full input


// get canvas
let ctx = document.getElementById('myChart').getContext('2d');


// input laser
let input_laser_power = 1
let input_laser_waist = 1
let input_laser_I = (2 * input_laser_power) / (Math.PI * input_laser_waist ** 2)

let saturation_I = 1.669

let s_0 = input_laser_I / saturation_I  // parametre de saturation
let n = 1  // η est un facteur de proportionnalit´e qui apparaˆıt entre le champ diffus´e et le dipˆole atomique
s_0 = 2

let total_intensity = n ** 2 * 0.5 * s_0 / (1 + s_0)  // L’intensit´e totale

let w_l = 0
// la r´eponse d’un dipˆole classique `a une excitation laser de fr´equence ωL. C’est donc en r´egime forc´e
// un rayonnement monochromatique de fr´equence ωL

let w_at = 0
// frequence de resonance

let G_b = 1
// Une des originalit´es de l’´echantillon atomique est le caract`ere r´esonant de la
// diffusion. Il est alors possible d’interpr´eter Γb**−1 comme le temps typique d’un evenement de diffusion,
// Γb designant la largeur naturelle de la transition

let d_l = w_l - w_at
// detuning


let graph_resolution = 1000
let grath_span = 20
let grath_step = grath_span / graph_resolution


let frequence_rabi = Math.sqrt(s_0 / 2)

let rabi_generalise = Math.sqrt(((G_b ** 2) / 2) * s_0 + d_l ** 2);


function elastic_intensity() {
    let new_s = s_0 / (1 + 4 * ((d_l / G_b) ** 2))
    return new_s / (2 * ((1 + new_s) ** 2))
}


function inelastic_intensity(w) {
    let d = w - w_l  // δ = ω − ωL.

    let d_l_g_b = (d_l / G_b) ** 2
    let d_g_b = (d / G_b) ** 2

    let first_part = n ** 2 * (1 / G_b)
    let second_part = (s_0 ** 2) / (8 * Math.PI * (1 + s_0 + 4 * d_l_g_b))

    let numerator_big_part = d_g_b + (s_0 / 4) + 1

    let denominator_big_part1 = (1 / 4) + s_0 / 4 + d_l_g_b - 2 * d_g_b

    let denominator_big_part2 = (5 / 4) + (s_0 / 2) + d_l_g_b - d_g_b

    let big_part = numerator_big_part / (denominator_big_part1 ** 2 + d_g_b * denominator_big_part2 ** 2)
    return first_part * second_part * big_part
}

const labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const data = {
    labels: labels,
    datasets: [{
        label: 'elactic+inelastic',
        data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        fill: true,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
    }, {
        label: 'elastic',
        data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        fill: false,
        borderColor: 'rgb(0, 192, 192)',
        tension: 0.1,
        hidden: true
    }, {
        label: 'inelastic',
        data: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        fill: false,
        hidden: true,
        borderColor: 'rgb(75, 192, 0)',
        tension: 0.1
    }]
};


function minValue(ctx) {
    const dataset = ctx.chart.data.datasets[0];
    return dataset.data.reduce((max, point) => Math.min(point, max), Infinity);


}

function maxValue(ctx) {
    const datasets = ctx.chart.data.datasets;
    const count = datasets[0].data.length;
    let max = 0;
    for (let i = 0; i < count; i++) {
        let sum = 0;
        for (const dataset of datasets) {
            sum += dataset.data[i];
        }
        max = Math.max(max, sum);
    }
    return max;
}

function if_dl(val) {
    return Math.abs(val - d_l) < grath_step;
}

function get_d_l(ctx) {
    const dataset = ctx.chart.data.labels;
    return dataset.findIndex(if_dl)

}

function if_wl(val) {


    return Math.abs(val - w_l) < grath_step;

}

function if_wl_near(val){
    return val==w_l
}

function get_w_l(ctx) {
    const dataset = ctx.chart.data.labels;
    let index = dataset.findIndex(if_wl_near)
    if (index === -1) {
        return dataset.findIndex(if_wl)
    }
    return index

}


function if_wat(val) {
    if (val === 0) {
        return true;
    } else {
        return Math.abs(val - 0) < grath_step;
    }
}

function get_w_at(ctx) {
    const dataset = ctx.chart.data.labels;
    let index = dataset.indexOf(0)
    if (index === -1) {
        return dataset.findIndex(if_wat)
    }
    return index

}

//
// const delta_l_annotation = {
//     type: 'line',
//     scaleID: 'x',
//     borderWidth: 3,
//     borderColor: 'black',
//     value: get_d_l,
//     label: {
//         rotation: 'auto',
//         backgroundColor: 'black',
//         content: (ctx) => "𝛿_L",
//         enabled: true
//     }
// };

const w_l_annotation = {
    type: 'line',
    scaleID: 'x',
    borderWidth: 3,
    borderColor: 'black',
    value: get_w_l,
    label: {
        rotation: 'auto',
        backgroundColor: 'black',
        content: (ctx) => "ωₗL",
        enabled: true,
        yAdjust: 50,
        font: {
            size: 20,
        }
    }
};


const wat_annotation = {
    type: 'line',
    scaleID: 'x',
    borderWidth: 3,
    borderColor: 'black',
    value: get_w_at,
    label: {
        rotation: 'auto',
        backgroundColor: 'black',
        content: (ctx) => "ωₐₜ",
        enabled: true,
        yAdjust: -50,
        font: {
            size: 20,
        }
    }
};

const annotation = {
    annotations: {
        // delta_l_annotation: delta_l_annotation,
        wat_annotation: wat_annotation,
        w_l_annotation: w_l_annotation,
    }
}


const plugins = {
    zoom: {
        limits: {
            y: {min: 0}
        },
        zoom: {
            wheel: {
                enabled: true,
            },
            pinch: {
                enabled: true,
            },
            mode: 'xy',
        },
        pan: {
            enabled: true,
            mode: 'xy',
        },
    },
    annotation: annotation


}

const options = {
    radius: 0,
    plugins: plugins,
    scales: {
        y: {
            stacked: false,
            display: true,
            title: {
                display: true,
                text: "Spectrum",
                font: {
                    family: 'Times',
                    size: 40,
                    style: 'normal',
                    lineHeight: 1.2
                },
            }
        },
        x: {
            stacked: false,
            display: true,
            ticks: {
                maxTicksLimit: 20,
                // callback: function (value, index, values) {
                //     return value
                // }
            },
            title: {
                display: true,
                text: "ω/Γb",
                font: {
                    family: 'Times',
                    size: 40,
                    style: 'normal',
                    lineHeight: 1.2
                },
            }
        }
    }
}

const config = {
    type: 'line',
    data: data,
    options: options,
}

const myChart = new Chart(ctx, config);
const saturation_input_I = document.getElementById('saturation_I');
const saturation_input = document.getElementById('s_0');
const input_laser_I_input = document.getElementById('input_laser_I');
const input_laser_waist_input = document.getElementById('input_laser_waist');
const input_laser_power_input = document.getElementById('input_laser_power');
const w_l_input = document.getElementById('w_l');
// const w_at_input = document.getElementById('w_at');
const n_input = document.getElementById('n');
const graph_span_input = document.getElementById('graph_span');
const G_b_input = document.getElementById('G_b');
const reset_zoom_input = document.getElementById('reset_zoom');
const frequence_rabi_input = document.getElementById("frequence_rabi");
const wat_anotation_input = document.getElementById("wo_anotation");
// const delta_l_anotation_input = document.getElementById("delta_l_anotation");
const delta_lorw_0_input = document.getElementById("delta_lorw_0")
// const delta_l_value_show = document.getElementById("delta_l_value")
const rabi_generalise_show = document.getElementById("rabi_generalisee")
const graph_resolution_input = document.getElementById("graph_resolution")
const w_l_annotation_show = document.getElementById("w_l_anotation")

let delta_lorw_0 = false

//setting defaults
input_laser_power_input.value = input_laser_power
input_laser_waist_input.value = input_laser_waist
saturation_input_I.value = saturation_I
w_l_input.value = w_l
// w_at_input.value = w_at
n_input.value = n
graph_span_input.value = grath_span
G_b_input.value = G_b
graph_resolution_input.value = graph_resolution
wat_anotation_input.value= myChart.config.options.plugins.annotation.annotations.wat_annotation.display
w_l_annotation_show.value=  myChart.config.options.plugins.annotation.annotations.w_l_annotation.display

function add_value_to_chart(ctx, x_pos, value, dataset_index = 0) {
    function is_x_pos(val){
        return val==x_pos
    }


    let x_values = myChart.data.labels
    let y_values = myChart.data.datasets[dataset_index].data
    let x_index = x_values.findIndex(is_x_pos)
    if (x_index === -1) {
        x_values.push(x_pos)
        x_values.sort((a, b) => a - b);
        x_index = x_values.indexOf(x_pos)
    }
    y_values[x_index] = value
    console.log(value)
    console.log(y_values[x_index])
}


function update_graph() {

    let x_values = []
    let y_values = []
    console.log(typeof (s_0))
    // if (s===""){
    //     s=1
    // }
    d_l = w_l - w_at
    console.log(d_l)
    if (input_laser_I_input.value === "") {
        input_laser_I = (2 * input_laser_power) / (Math.PI * input_laser_waist ** 2)
        input_laser_I_input.placeholder = input_laser_I.toFixed(2)
        console.log(input_laser_power)
        console.log(input_laser_waist)
    } else {
    }
    // d_l = 0;
    if (saturation_input.value === "") {

        s_0 = input_laser_I / saturation_I
        saturation_input.placeholder = s_0.toFixed(2);


    } else {
        // saturation_estimation.innerText = ""

    }
    if (frequence_rabi_input.value === "") {
        frequence_rabi = Math.sqrt(s_0 / 2)
        frequence_rabi_input.placeholder = frequence_rabi.toFixed(2)
    } else {
        s_0 = (frequence_rabi ** 2) * 2
        saturation_input.placeholder = s_0.toFixed(2);
    }

    console.log(s_0)

    let offset = 0
    grath_step = grath_span / graph_resolution
    if (delta_lorw_0) {
        offset = w_l
    }
    rabi_generalise = Math.sqrt(((G_b ** 2) / 2) * s_0 + d_l ** 2)

    let grath_start = -grath_span +offset
    let grath_end = grath_span + offset
    let zero_array = []

    for (let i = grath_start; i < grath_end; i += grath_step) {
        x_values.push(i.toFixed(4))
        y_values.push(inelastic_intensity(i))
        zero_array.push(0)
    }


    myChart.data.labels = x_values;

    myChart.data.datasets[0].data = y_values;
    myChart.data.datasets[1].data = zero_array;
    myChart.data.datasets[2].data = [...y_values];
    add_value_to_chart(ctx, 0, inelastic_intensity(0), 0)
    add_value_to_chart(ctx, 0, inelastic_intensity(0), 2)
    add_value_to_chart(ctx, w_l, inelastic_intensity(w_l) + elastic_intensity())
    add_value_to_chart(ctx, w_l, elastic_intensity(), 1)

    // console.log("elastic is " + inelastic_intensity(w_l) + elastic_intensity())
    // console.log(elastic_intensity())
    // console.log("elastic is " + myChart.data.datasets[0].data[myChart.data.labels.indexOf(w_l)] )
    myChart.update();


}

update_graph()
console.log(myChart)

saturation_input_I.oninput = function () {
    saturation_I = parseFloat(this.value);
    update_graph();
}

// get s input1

saturation_input.oninput = function () {
    s_0 = parseFloat(this.value);
    update_graph();
}


input_laser_I_input.oninput = function () {
    input_laser_I = parseFloat(this.value);
    update_graph();
}


input_laser_waist_input.oninput = function () {
    input_laser_waist = parseFloat(this.value);
    update_graph();
}

input_laser_power_input.oninput = function () {
    input_laser_power = parseFloat(this.value);
    update_graph();
}


graph_span_input.oninput = function () {
    grath_span = parseFloat(this.value);
    update_graph();
}

// w_at_input.oninput = function () {
//     w_at = parseFloat(this.value);
//     update_graph();
// }

w_l_input.oninput = function () {
    w_l = parseFloat(this.value);
    update_graph();
}

n_input.oninput = function () {
    n = parseFloat(this.value);
    update_graph();
}

G_b_input.oninput = function () {
    G_b = parseFloat(this.value);
    update_graph();
}

frequence_rabi_input.oninput = function () {
    frequence_rabi = parseFloat(this.value);
    update_graph();
}

wat_anotation_input.onclick = function () {
    myChart.config.options.plugins.annotation.annotations.wat_annotation.display = this.checked;
    update_graph();
}

// delta_l_anotation_input.onclick = function () {
//     myChart.config.options.plugins.annotation.annotations.delta_l_annotation.display = this.checked;
//     update_graph();
// }
w_l_annotation_show.onclick = function () {
    myChart.config.options.plugins.annotation.annotations.w_l_annotation.display = this.checked;
    update_graph();
}

delta_lorw_0_input.onclick = function () {
    delta_lorw_0 = this.checked;
    update_graph();

}

graph_resolution_input.oninput = function () {
    graph_resolution = graph_resolution_input.value
    update_graph();
}

reset_zoom_input.onclick = function () {
    console.log("cliked")
    console.log(myChart)
    myChart.resetZoom()
    // ChartZoom.resetZoom(myChart)
    // // resetZoom(myChart);
    // // resetPan(myChart)
}
console.log(reset_zoom_input)

function update_inputs() {
    // delta_l_value_show.innerText = d_l;
    rabi_generalise_show.innerText = rabi_generalise.toFixed(2)
    if (frequence_rabi_input.value !== "") {
        saturation_input.disabled = true;
        input_laser_power_input.disabled = true;
        input_laser_waist_input.disabled = true;
        saturation_input_I.disabled = true;
        input_laser_I_input.disabled = true;
    } else {
        saturation_input.disabled = false;

        if (saturation_input.value !== "") {
            input_laser_power_input.disabled = true;
            input_laser_waist_input.disabled = true;
            saturation_input_I.disabled = true;
            input_laser_I_input.disabled = true;
        } else {

            saturation_input_I.disabled = false;
            input_laser_I_input.disabled = false;

            if (input_laser_I_input.value !== "") {
                input_laser_power_input.disabled = true;
                input_laser_waist_input.disabled = true;
            } else {
                input_laser_power_input.disabled = false;
                input_laser_waist_input.disabled = false;
            }


        }
    }
}

update_graph()


setInterval(update_inputs, 0)
